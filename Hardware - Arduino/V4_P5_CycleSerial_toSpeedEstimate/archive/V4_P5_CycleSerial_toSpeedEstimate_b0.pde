import processing.serial.*;

Serial myPort;        // The serial port

float estimatedVelocity_mps = 0.0;

void setup () {

//  frameRate(1); // does well even with low frameRate (as in the Serial values will still be captured).

  // set the window size:
  size(400, 300);

  // List all the available serial ports
  // if using Processing 2.1 or later, use Serial.printArray() // eh - no
  println(Serial.list());

  // I know that the first port in the serial list on my mac
  // is always my  Arduino, so I open Serial.list()[0].
  // Open whatever port is the one you're using.
  //  myPort = new Serial(this, Serial.list()[0], 9600);
  myPort = new Serial(this, "/dev/tty.usbmodemfa131", 9600);

  // don't generate a serialEvent() unless you get a newline character:
  myPort.bufferUntil('\n');

  // set inital background:
  background(0);
  textAlign(LEFT, TOP);
  textSize(64);
  text("loaded", 0, 0);
}

void draw() {
  // draw loop must be present to run every frame
  background(0);
  text(estimatedVelocity_mps, 0, 0);
  
  // Update
  if (millis() > (lastSignalTime_ms + timeBeforeSpeedDecrease_ms) ) {
    estimatedVelocity_mps -= 0.01;
  }
  estimatedVelocity_mps = Math.max(0, estimatedVelocity_mps);
}

float lastSignalTime_ms = 0;
float timeBeforeSpeedDecrease_ms = 1000;

void serialEvent (Serial myPort) {

  // get the ASCII string:
  String inString = myPort.readStringUntil('\n');

  if (inString != null) {
    // trim off any whitespace:
    inString = trim(inString);
    // convert to an int and map to the screen height:
    float inByte = float(inString);
    println(inByte);

    background(70);
    
    // update values
    timeBeforeSpeedDecrease_ms = millis() - lastSignalTime_ms;
    timeBeforeSpeedDecrease_ms = Math.min(1000*4, timeBeforeSpeedDecrease_ms);
    lastSignalTime_ms = millis();
    
    estimatedVelocity_mps = inByte;
    
  }
}

