/* Using the Arduino software, upload the StandardFirmata example (located
 in [Arduino] Examples > Firmata > StandardFirmata) to your Arduino board.
 */
 
 // NOTE - UNUSED - TOO SLOW, SIGNALS ARE MISSED - NEED TO TRY ALTERNATIVE METHOD LIKE OSC

// Arduino Libraries
import processing.serial.*;
import cc.arduino.*;

// Arduino
Arduino arduino;

int arduino_baudRate = 57600; //57600; // 9600;

int pinSwitch = 12;

void setup() {
  size(500,500);
  // frameRate(15); // rough estimation of sketch performance - see if values are still captured...

  // Prints out the available serial ports.
  println(Arduino.list());

  // Modify this line, by changing the "0" to the index of the serial
  // port corresponding to your Arduino board (as it appears in the list
  // printed by the line above).
  arduino = new Arduino(this, Arduino.list()[0], arduino_baudRate);

  // Alternatively, use the name of the serial port corresponding to your
  // Arduino (in double-quotes), as in the following line.
  //arduino = new Arduino(this, "/dev/tty.usbmodem621", 57600);
  arduino = new Arduino(this, "/dev/tty.usbmodemfa131", arduino_baudRate);

  // SETUP REED SWITCH
  arduino.pinMode(pinSwitch, Arduino.INPUT);

  //
  noStroke();
}

void draw() {

  fill(0, 1);
  rect(0, 0, width, height);

  int curSwitchValue = arduino.digitalRead(pinSwitch);
  println(curSwitchValue);
  if (curSwitchValue == 1) {
    //    background(255);
    fill(255);
    rect(0, 0, width, height);
  } else {
    //    background(0);
  }
}

