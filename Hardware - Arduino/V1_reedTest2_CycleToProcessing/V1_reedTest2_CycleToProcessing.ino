const int pinSwitch = 12; //Pin Reed

int prvSwitchValue = 0;
int curSwitchValue = 0;

void setup() {

  Serial.begin(9600);

  pinMode(pinSwitch, INPUT);

}

int switchCount = 0;

long lastClick_ms = 0;

float sensorRadius_m = 0.27; //m
float distanceTravelledPerRevolution_m = 2 * 3.14159 * sensorRadius_m;


void loop() {

  curSwitchValue = digitalRead(pinSwitch);

  if (curSwitchValue == 1 && prvSwitchValue != 1) {
    long curClick_ms = millis();
    long elapsedTime_ms = curClick_ms - lastClick_ms;

    if (elapsedTime_ms > 20) {
      // at the fastest riding speed I only get ~240ms
      // sometimes see erroneous fires of 0, 1, and 4.
      //      Serial.println(elapsedTime_ms);

      float velocity_mps = distanceTravelledPerRevolution_m / ( ((float) elapsedTime_ms) / 1000);

      Serial.println(velocity_mps);
      // Calculations actually should be done in Processing - for flexibility of manipulating the smoothing of speed - for now though, curious on what the actual calculation is

    }

    // update
    lastClick_ms = curClick_ms;
  }

  // update
  prvSwitchValue = curSwitchValue;

}






