http://www.hanleyweng.com/#/riding-live/

# Project Description #

Riding Live prompts participants to ride a stationary bicycle to control their journey through an immersive landscape. It aims to encourage the growth and recognition of bike culture.

# README #

Includes a Processing (processing.org) file that can be run to show the visuals. (Flythrough.pde)

StargateDrawer.pde can be used to switch the mode between ixMode_MOUSE, or ixMode_BIKE (using bike sensors connected to an arduino).

The latest hardware sketch for the arduino is listed in "V4_P5_CycleSerial_toSpeedEstimate". It reads a reed sensor attached to the bike.

See video here for a short snippet of the setup.

http://www.hanleyweng.com/#/riding-live/