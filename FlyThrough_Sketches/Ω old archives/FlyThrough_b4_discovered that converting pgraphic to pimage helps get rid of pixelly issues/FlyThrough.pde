
PImage texture;
PShader myShader;

PGraphics wallGraphic;
int wallGraphicWidth = 1280;
int wallGraphicHeight = 720;

PGraphics stargateGraphic;

PImage bannerImage2;
PGraphics bannerImage2copy;
PImage bannerImage2copycopy;

int swidth = 1280;
int sheight = 720;

boolean debugMode = false;

void setup() {
  size(swidth, sheight, P2D);

  wallGraphic = createGraphics(wallGraphicWidth, wallGraphicHeight, P2D);
  wallGraphic.textureWrap(REPEAT);
  //  wallGraphic.smooth(8);

  bannerImage2 = loadImage("bannerTest2.jpg");
  bannerImage2copy = createGraphics(bannerImage2.width, bannerImage2.height, P2D);
  bannerImage2copy.beginDraw();
  bannerImage2copy.image(bannerImage2, 0, 0);
  bannerImage2copy.endDraw();
  
  bannerImage2copycopy = bannerImage2copy.get();

  stargateGraphic = createGraphics(swidth, sheight, P2D);
  stargateGraphic.textureWrap(REPEAT);
  //  stargateGraphic.smooth(8);
  //
  //  textureWrap(REPEAT);
  //
  myShader = loadShader("fly.glsl");

  myShader.set("resolution", float(swidth), float(sheight));
}

int currentFrame = 0;

void draw() {
  //  background(122);

  //  ellipse(swidth/2, sheight/2, 50, 50);

  ///////////////////////////////////////////////////////
  // Update Shader
  myShader.set("time", millis() / 1000.0);

  ///////////////////////////////////////////////////////
  // Draw
  //  if (!debugMode) shader(myShader);

  drawWallGraphic(wallGraphic);

  //  if (debugMode) {
  //    image(wallGraphic, 0, 0); // Note - Image must stretch canvas in order to be applied
  //  } else {
  //    image(wallGraphic, 0, 0, swidth, sheight);
  //  }
  //
  //  resetShader();

  stargateGraphic.beginDraw();
  //  stargateGraphic.shader(myShader);
  stargateGraphic.image(wallGraphic, 0, 0, swidth, sheight);
  //  stargateGraphic.filter(myShader); // filter annoyingly leaves a little black circle near the horizon line
  stargateGraphic.endDraw();

  image(stargateGraphic, 0, 0);

  ///////////////////////////////////////////////////////
  // Update
  currentFrame++;
}

void drawWallGraphic(PGraphics g) {
  g.beginDraw();
  // setup
  g.colorMode(HSB, 1.0);
  //  g.background(0.5); // background doesn't seem to work, drawing my own
  // draw debug - boundaries
  g.fill(0.1);
  g.stroke(1, 0, 1);
  g.rect(0, 0, g.width, g.height);

  // position
  float progressionPerFrame = 1.0; // shouldn't be less than 1
  int loopDuration =(int)( g.width*1f/progressionPerFrame);

  float cx = g.width - (currentFrame%loopDuration)*1f/loopDuration * g.width;
  float cy = g.height/2;

  //  cx = 0;

  g.shader(myShader); // NOTE: In order for shader to nicely affect images ,those images can not be PGraphics!

  // draw image
  g.image(bannerImage2copycopy, 0, 0, swidth, sheight);
  //  g.image(bannerImage2, cx, 0, swidth, sheight);
  //  g.image(bannerImage2, cx-g.width, 0, swidth, sheight);

  g.resetShader();

  //  // draw rect
  //  g.noFill();
  //  g.rect(0, 0, g.width, g.height);

  // draw circle
  g.strokeWeight(5);
  g.noFill();
  //  g.translate(cx, cy);
  g.translate(0, cy);
  g.ellipse(0, 0, 100, 100);

  //  g.filter(myShader);

  g.endDraw();
}

void mouseClicked() {
  debugMode = !debugMode;
}

