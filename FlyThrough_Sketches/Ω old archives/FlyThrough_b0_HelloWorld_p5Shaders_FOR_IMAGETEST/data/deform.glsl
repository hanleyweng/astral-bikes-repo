// Original Reference; iq : http://www.iquilezles.org/apps/shadertoy/index2.html - DEFORM
// Shader Toy Reference: https://www.shadertoy.com/view/4sXGzn

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

uniform sampler2D texture;

uniform float time; // runtime in float seconds
uniform vec2 resolution; // (swidth, sheight) - pixels
uniform vec2 mouse; // (mouseX, mouseY) - pixels

void main(void) {
  // gl_FragCoord.xy - pixel location in pixels

  // Get pixel location as range [0,1]
  vec2 p = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;

  // Get mouse location as range [0,1]
  vec2 m = -1.0 + 2.0 * mouse.xy / resolution.xy;

  // Some deformation calculations
  float a1 = atan(p.y - m.y, p.x - m.x);
  float r1 = sqrt(dot(p - m, p - m));
  float a2 = atan(p.y + m.y, p.x + m.x);
  float r2 = sqrt(dot(p + m, p + m));

  // ~ Determine 'position' of pixel on the texture
  vec2 uv;
  uv.x = 0.2 * time + (r1 - r2) * 0.25;
  uv.y = sin(2.0 * (a1 - a2));

  // ~ Determine color of pixel from texture and UV
  float w = r1 * r2 * 0.8; // ~ smaller makes it more glowy
  vec3 col = texture2D(texture, 0.5 - 0.495 * uv).xyz;

  // set the gl_FragColor - i.e. pixel color)
  gl_FragColor = vec4(col / (0.1 + w), 1.0);
}