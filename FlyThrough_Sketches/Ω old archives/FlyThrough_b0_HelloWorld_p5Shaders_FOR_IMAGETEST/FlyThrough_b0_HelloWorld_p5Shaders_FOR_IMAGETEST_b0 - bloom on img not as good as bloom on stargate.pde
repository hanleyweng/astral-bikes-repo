
PImage texture;
PShader myShader;
PShader postProcessShader;

PGraphics pass1, pass2;

int swidth = 1280;
int sheight = 720;

void setup() {
  size(swidth, sheight, P2D);

  textureWrap(REPEAT);

  pass1 = createGraphics(swidth, sheight, P2D);
  pass2 = createGraphics(swidth, sheight, P2D);

  // CHANGE TEXTURE HERE
  //  texture = loadImage("tex1.jpg");
  // texture = loadImage("bannerTest1.jpg");
  texture = loadImage("bannerTest2.jpg");

  // FlyThrough Shader
  myShader = loadShader("fly.glsl");
  myShader.set("resolution", float(swidth), float(sheight));

  // Default Blur 1
  //  postProcessShader = loadShader("sepBlur.glsl");
  //  postProcessShader.set("blurSize", 9);
  //  postProcessShader.set("sigma", 5.0f);  

  // Default Blur 2
  //  postProcessShader = loadShader("blurFilter.glsl");

  // Bloom Shader
  postProcessShader = loadShader("bloom.glsl");
  postProcessShader.set("iResolution", float(swidth), float(sheight));
}

void draw() {
  background(0);



  // Shader - PostProcess
  postProcessShader.set("iMouse", float(mouseX), float(mouseY));
  pass2.beginDraw();
  if (!mousePressed) {
    pass2.shader(postProcessShader);
  }
//  pass2.image(pass1, 0, 0);
  pass2.image(texture, 0, 0, swidth, sheight);
  pass2.resetShader();
  pass2.endDraw();

  // Shader - Stargate
  myShader.set("time", millis() / 1000.0);
  pass1.beginDraw();
  pass1.shader(myShader);
  pass1.textureWrap(REPEAT);
//  pass1.image(texture, 0, 0, swidth, sheight);
  pass1.image(pass2,0,0);
  pass1.endDraw();

  image(pass1, 0, 0);
}

