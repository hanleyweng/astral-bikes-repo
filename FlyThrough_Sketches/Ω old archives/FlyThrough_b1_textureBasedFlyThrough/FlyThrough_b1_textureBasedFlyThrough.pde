
PImage texture;
PShader myShader;

PGraphics wallGraphic;
int wallGraphicWidth = 512;
int wallGraphicHeight = 512;

int swidth = 1024; // make this flexible!
int sheight = 812;

boolean debugMode = false;

void setup() {
  size(swidth, sheight, P2D);

  wallGraphic = createGraphics(wallGraphicWidth, wallGraphicHeight, P2D);

  //
  textureWrap(REPEAT);
  texture = loadImage("tex1.jpg");
  //
  myShader = loadShader("fly.glsl");
  println(width, height);
  myShader.set("resolution", float(swidth), float(sheight));
}

int currentFrame = 0;

void draw() {
  background(122);

  ///////////////////////////////////////////////////////
  // Update Shader
  myShader.set("time", millis() / 1000.0);

  ///////////////////////////////////////////////////////
  // Draw
  if (!debugMode) shader(myShader);

  drawWallGraphic(wallGraphic);
  
  if (debugMode) {
    image(wallGraphic, 0, 0); // Note - Image must stretch canvas in order to be applied
  } else {
    image(wallGraphic, 0, 0, swidth, sheight);
  }

  resetShader();
  
  ///////////////////////////////////////////////////////
  // Update
  currentFrame++;
}

void drawWallGraphic(PGraphics g) {
  g.beginDraw();
  // setup
  g.colorMode(HSB, 1.0);
  //  g.background(0.5); // background doesn't seem to work, drawing my own
  // draw debug - boundaries
  g.fill(0.1);

  g.stroke(1, 0, 1);
  g.rect(0, 0, g.width, g.height);

  // circle position
  int loopDuration = 100;
  float cy = (currentFrame%loopDuration)*1f/loopDuration * g.height;

  // draw centre circle
  g.strokeWeight(5);
  g.translate(g.width/2, cy);
  g.ellipse(0, 0, 30, 30);

  g.endDraw();
}

void mouseClicked() {
  debugMode = !debugMode;
}

