import processing.serial.*; // Arduino Serial Port

// ARDUINO
Serial arduinoPort;
float ix_estimatedVelocity_mps = 0.0;
float ix_targetVelocity_mps = 0.0; // this is the 'real' velocity
float ix_speedEffect_n = 0.0;
float estVelocity_speedIncreaseToTarget = 0.03;
float estVelocity_speedDecreaseToTarget = 0.05;
float tmp_lastSignalTime_ms = 0;
float tmp_timeBeforeSpeedDecrease_ms = 1000; // var

// SETUP  
void setupArduino() {
  // List all the available serial ports - use these to set the port
  println("Available Arduino serial ports: ");
  println(Serial.list());

  // set port
  arduinoPort = new Serial(this, "/dev/tty.usbmodemfa131", 9600);

  // don't generate a serialEvent() unless you get a newline character:
  arduinoPort.bufferUntil('\n');
}

// UPDATE
void updateFromArduino() {
  // Update VELOCITY
  // move estimated velocity towards target
  if (ix_estimatedVelocity_mps < ix_targetVelocity_mps) {
    ix_estimatedVelocity_mps += estVelocity_speedIncreaseToTarget;
    ix_estimatedVelocity_mps = Math.min(ix_estimatedVelocity_mps, ix_targetVelocity_mps);
  }
  if (ix_estimatedVelocity_mps > ix_targetVelocity_mps) {
    ix_estimatedVelocity_mps -= estVelocity_speedDecreaseToTarget;
    ix_estimatedVelocity_mps = Math.max(ix_estimatedVelocity_mps, ix_targetVelocity_mps);
  }

  // If there hasn't been any new input for a while (in accordance to last speed registered) - set targetVelocity to 0
  if (millis() > (tmp_lastSignalTime_ms + tmp_timeBeforeSpeedDecrease_ms) ) {
    ix_targetVelocity_mps = 0.0;
  }

  // UPDATE EFFECT_N
  float effect_n = map(ix_estimatedVelocity_mps, 0, 8, 0, 1);
  effect_n = Math.min(effect_n, 1.0);

  ix_speedEffect_n = effect_n;
}

// SERIAL EVENT
void serialEvent (Serial myPort) {

  // get the ASCII string:
  String inString = myPort.readStringUntil('\n');

  if (inString != null) {
    // trim off any whitespace:
    inString = trim(inString);
    // convert to an int and map to the screen height:
    float inByte = float(inString);
    //    println(inByte);

    // update values
    tmp_timeBeforeSpeedDecrease_ms = millis() - tmp_lastSignalTime_ms;
    tmp_timeBeforeSpeedDecrease_ms = Math.min(1000*4, tmp_timeBeforeSpeedDecrease_ms);
    tmp_lastSignalTime_ms = millis();

    ix_targetVelocity_mps = inByte;
  }
}
