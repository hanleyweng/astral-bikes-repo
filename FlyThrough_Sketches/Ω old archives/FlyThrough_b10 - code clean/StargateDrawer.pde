
class StargateDrawer {
  // NOTES
  // efficiency - framerate can be increased by changing scales, and drawing less pshapes (drawing textured images seems ok)
  // filter leaves a black circle near the horizon line, potentially because values are infinite near there
  // shaders and filters don't play nice with the pixels in PGraphic, they're fine with PImages though

  PGraphics flatViewGround;
  PImage flatViewGroundBgTexture; 
  PGraphics flatSky;
  PImage flatSkyBgTexture;

  PShader warpShader;
  PShader bloomShader;

  PGraphics warpedGroundGraphic;
  PGraphics warpedSkyGraphic;
  PGraphics bloomedGraphic;
  PGraphics outputGraphic;

  ParticleField groundParticleField;
  ParticleField skyParticleField;

  StargateDrawer(int swidth, int sheight) {

    // SETUP FLAT VIEWS
    flatViewGround = createGraphics(swidth, sheight, P2D); // these dimensions can be changed
    flatViewGround.textureWrap(REPEAT);
    flatViewGround.smooth(8);

    flatViewGroundBgTexture = loadImage("bannerTest1.jpg");
    flatViewGroundBgTexture = loadImage("bannerTest2.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest3d_fla.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4b.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4d.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4e.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4e2.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4f.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4f_r.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4g.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest4g_r.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest5b.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest6.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest6c.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest7.jpg");
    //    flatViewGroundBgTexture = loadImage("bannerTest8.jpg");
    //
    //    flatViewGroundBgTexture = loadImage("image1.png");
    //    flatViewGroundBgTexture = loadImage("image2.png");
    //    flatViewGroundBgTexture = loadImage("image3.png");
    //    flatViewGroundBgTexture = loadImage("image4.png");

    flatSky = createGraphics(swidth, sheight, P2D); // these dimensions can be changed
    flatSky.textureWrap(REPEAT);
    flatSky.smooth(8);
    //    flatSkyBgTexture = loadImage("bannerTest2_sky.jpg");
    //    flatSkyBgTexture = loadImage("bannerTest2.jpg");
    //    flatSkyBgTexture = loadImage("image2.png");
    flatSkyBgTexture = flatViewGroundBgTexture;

    // setup shaders
    warpShader = loadShader("stargateWarp.glsl");
    warpShader.set("resolution", float(swidth), float(sheight));

    bloomShader = loadShader("basicBloom.glsl");
    bloomShader.set("iResolution", float(swidth), float(sheight));

    // Create Graphics
    warpedGroundGraphic = createGraphics(swidth, sheight, P2D);
    warpedGroundGraphic.textureWrap(REPEAT);

    warpedSkyGraphic = createGraphics(swidth, sheight, P2D);
    warpedSkyGraphic.textureWrap(REPEAT);

    bloomedGraphic = createGraphics(swidth, sheight, P2D);

    outputGraphic = createGraphics(swidth, sheight);

    // setup particle field
    groundParticleField = new ParticleField();
    skyParticleField = new ParticleField();
  }

  int loopDuration_ms = 20*1000;

  // UPDATE
  void update() {
    // Set Speed-Dependent Variables



    float mouseX_n = mouseX*1f/swidth;
    mouseX_n = Math.max(0, mouseX_n);
    //    mouseX_n = (float) (Math.pow(mouseX_n, 0.1)) ;

    //    speedEffect_n = mouseX_n;

    //    speedEffect_n = ix_speedEffect_n;

    speedEffect_n = (float) (Math.pow(ix_speedEffect_n, 0.5));

    // Update Textured Background's loop duration
    int[] loopDuration_ms_range = {
      100*1000, 5*1000
    };
    loopDuration_ms = (int)(map(speedEffect_n, 0, 1, loopDuration_ms_range[0], loopDuration_ms_range[1]));

    // Update Warpe Scale
    // default is .001
    float[] warp_wrapScaleHeight_range = { // this handles the speed looking effect mostly and most visual interest tbh - don't need actual increases in speed really
      //      0.08, 0.0005
      0.0300, 0.0005
    };
    //    float speedEffect_n_pow = (float) (Math.pow(speedEffect_n, 2));
    float warp_wrapScaleHeight = map(speedEffect_n, 0, 1, warp_wrapScaleHeight_range[0], warp_wrapScaleHeight_range[1]);
    //        warp_wrapScaleHeight = 0.005;
    warpShader.set("textureScaleWidth", warp_wrapScaleHeight);

    // Update Speed Increase Slightly
    float[] textureXPos_speedRange = {
      0.0002, 0.0020
    };
    textureXVel_n = map(speedEffect_n, 0, 1, textureXPos_speedRange[0], textureXPos_speedRange[1]);

    // Update Flat Views
    updateFlatView(flatSky, 0);
    updateFlatView(flatViewGround, 1);
  }
  void mouseReleased() {
    debugMode += 1;
    debugMode %= debugModes.length;
  }

  float textureXPos_n = 0.0;
  float textureXVel_n = 0.0005; // min 0.0005  max 0.002
  void updateFlatView(PGraphics g, int planeType) {

    // Update textureXPos_n
    textureXPos_n -= textureXVel_n;
    // restrict textureXPos_n to betweeen 1.0, 0.0
    textureXPos_n = (textureXPos_n+1.0) % 1.0;

    //
    ParticleField particleField = (planeType==0) ? skyParticleField : groundParticleField;
    particleField.update();

    // planeType = 0 for sky, = 0 for ground
    g.beginDraw();
    g.colorMode(HSB, 1.0);
    g.fill(0.1);
    g.noStroke();
    g.rect(0, 0, g.width, g.height);

    // determine zoom position
    // cx should be determined by a persistent moving position that is effected by speedEffect_n - not some loopDuration
    //    float cx = g.width - (millis()%loopDuration_ms)*1f/loopDuration_ms * g.width;
    //    float cy = g.height/2;

    // draw scrolling background image
    PImage backgroundTexture = (planeType ==0)? flatSkyBgTexture : flatViewGroundBgTexture;

    float textureXscale = 0.5; //1.0; //0.5; //0.25; // 0.15
    float textureWidth = backgroundTexture.width * textureXscale; // backgroundTexture.width; //g.width;

    //    float textureXPos_n = 1 - (millis()%loopDuration_ms)*1f/loopDuration_ms; 
    float textureXPos = textureXPos_n * textureWidth;

    //    g.image(backgroundTexture, textureXPos, 0, textureWidth, sheight);

    // Draw textures repeatedly if they don't wrap around
    int numTexturesRepeated = (int) (g.width *1f / textureWidth) + 1;
    for (int i=0; i<=numTexturesRepeated; i++) {
      g.image(backgroundTexture, textureXPos + textureWidth * (i-1), 0, textureWidth, sheight);
    }




    // draw debug rectangle boundaries
    //  g.noFill(); g.rect(0, 0, g.width, g.height);

    // draw circle
    //    g.stroke(1, 0, 1);
    //    g.strokeWeight(1);
    //    g.noFill();
    //    float x, y;
    //    for (int i=0; i<10; i++) {
    //      x = i*80; //cx;
    //      y = cy;
    //      g.ellipse(x, y, 40, 40);
    //    }

    // draw particles onto flat view
    //    g.stroke(1, 0, 1);
    //    g.strokeWeight(2.0); // vary by speed
    //    ArrayList<Particle> particles = particleField.getParticles();
    //    for (Particle p : particles) {
    //      g.point(p.x * g.width, p.y * g.height);
    //    }

    g.endDraw();
  }

  String[] debugModes = {
    "normal", "flatviewGround"
  };
  int debugMode = 0;

  PGraphics getOutputGraphic() {

    String curDebugMode = debugModes[debugMode];


    if (curDebugMode.equals("flatviewGround")) {
      outputGraphic.beginDraw();
      outputGraphic.image(flatViewGround.get(), 0, 0, swidth, sheight/2);
      outputGraphic.image(flatSky.get(), 0, sheight/2, swidth, sheight/2);
      outputGraphic.endDraw();
      return outputGraphic;
    }

    // Warp Flat View
    // update warp shader
    warpShader.set("iMouse", float(mouseX), float(mouseY));
    warpShader.set("time", millis() / 1000.0); // update shader

    warpedGroundGraphic.beginDraw();
    warpShader.set("mode", 1);
    warpedGroundGraphic.shader(warpShader);
    warpedGroundGraphic.image(flatViewGround.get(), 0, 0, swidth, sheight);
    warpedGroundGraphic.endDraw();

    warpedSkyGraphic.beginDraw();
    warpShader.set("mode", 0);
    warpedSkyGraphic.shader(warpShader);
    warpedSkyGraphic.image(flatSky.get(), 0, 0, swidth, sheight);
    warpedSkyGraphic.endDraw();

    // Bloom
    bloomedGraphic.beginDraw();
    bloomShader.set("iMouse", float(mouseX), float(mouseY));
    bloomedGraphic.shader(bloomShader);
    bloomedGraphic.image(warpedGroundGraphic, 0, 0);
    bloomedGraphic.image(warpedSkyGraphic, 0, 0);
    bloomedGraphic.endDraw();

    // Output
    return bloomedGraphic;
  }
}

