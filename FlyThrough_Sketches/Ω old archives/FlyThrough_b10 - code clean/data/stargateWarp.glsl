// IQ Reference: http://www.iquilezles.org/apps/shadertoy/index2.html - FLY
// Shader Toy References:
// https://www.shadertoy.com/view/4s2XWm#
// https://www.shadertoy.com/view/4sBSRy#

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

uniform sampler2D texture; //

uniform float time; // runtime in float seconds
uniform vec2 resolution; // (swidth, sheight) - pixels
uniform vec2 iMouse; // mouse position - pixels

uniform float textureScaleWidth;
uniform int mode; // 0 = sky, 1 = ground, 2 = sky and ground

vec3 hsv2rgb (in vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

void main(void) {
  float _speed_horizonRotation = 0.0; //0.25; // 0.025;
  float textureScaleHeight = .08; //.05; //.1; // determines the 'width' scale of the image layed down (smaller the value longer the img )

  vec2 mouse_n = iMouse.xy/resolution.xy;

  // gl_FragCoord.xy = pixel location in pixels

  // Get pixel location normalized as range [0,1]
  vec2 pixelPos_n = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;

  // Horizon Rotation
  float angle = time * _speed_horizonRotation;

  // ~? Texture Position Calculations
  float x = pixelPos_n.x * cos(angle) - pixelPos_n.y * sin(angle);
  float y = pixelPos_n.x * sin(angle) + pixelPos_n.y * cos(angle);

  vec2 uv;
  // note: 1 / abs(y) - means its '1' at horizon edge, and infinite in between
  // note: swapped y and x to rotate image

  // Chromatic abberation Fringe effect
  // Swap out for original
  vec3 pixelColor_col = vec3(0.0);
  for (int i = 0; i < 3; i++) {
    float offset = (float(i) - 2.0) / 10000.0;
    uv.x = (textureScaleWidth + float(offset)) / abs(y);
    uv.y = textureScaleHeight * x / abs(y) + 0.5;
    pixelColor_col[i] = texture2D(texture, uv).x;
  }

  // Determine Pixel Color
  // vec3 pixelColor_col = texture2D(texture, uv).xyz;
  // uv.x = textureScaleWidth / abs(y);
  // uv.y = textureScaleHeight * x / abs(y) + 0.5;

  // pixelColor_col *= y*y; // this makes the texture visually fade into the z axis
  // pixelColor_col *= 0.09/abs(y); // this makes it get whiter towards the horizon
  // pixelColor_col *= max(0.1/abs(y), y*y); // combines the 2 above, making the horizon and the edges bright
  // pixelColor_col *= pow(abs(y), 0.6) * 10.0; // another way of making it darker near z axis
  // pixelColor_col *= hsv2rgb(vec3(mod(time / 1000.0, 360.0), 0.7, 1.0));

  float pixelColor_alpha = 1.0;
  // SKY MODE ONLY
  if (mode==0) {
    if (y > 0.0) {
      pixelColor_col = vec3(0.0,0.0,0.0);
      pixelColor_alpha = 0.0;
    }
  }
  // GROUND MODE ONLY
  if (mode==1) {
    if (y < 0.0) {
      pixelColor_col = vec3(0.0,0.0,0.0);
      pixelColor_alpha = 0.0;
    }
  }
  // SKY AND GROUND MODE - any other mode

  // set the gl_FragColor - i.e. pixel color
  gl_FragColor = vec4(pixelColor_col, pixelColor_alpha);
}