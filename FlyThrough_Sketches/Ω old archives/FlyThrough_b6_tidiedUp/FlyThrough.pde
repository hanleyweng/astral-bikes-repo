
int swidth = 1280;
int sheight = 800;

StargateDrawer stargateDrawer;

void setup() {
  size(swidth, sheight, P2D);

  // Set the target framerate to ridiculously high to see how fast we can actually get this
  frameRate(1000);

  // setup Stargate
  stargateDrawer = new StargateDrawer(swidth, sheight);
}

class StargateDrawer {
  // NOTES
  // efficiency - framerate can be increased by changing scales, and drawing less pshapes (drawing textured images seems ok)
  // filter leaves a black circle near the horizon line, potentially because values are infinite near there
  // shaders and filters don't play nice with the pixels in PGraphic, they're fine with PImages though

  PGraphics flatView;
  PImage flatViewBgTexture; 
  PShader warpShader;
  PShader bloomShader;

  PGraphics warpedGraphic;

  StargateDrawer(int swidth, int sheight) {

    // setup flatView
    int flatViewWidth = swidth; // these can be changed
    int flatViewHeight = sheight;
    flatView = createGraphics(flatViewWidth, flatViewHeight, P2D);
    flatView.textureWrap(REPEAT);
    flatView.smooth(8);

    flatViewBgTexture = loadImage("bannerTest2.jpg");

    // setup shaders
    warpShader = loadShader("stargateWarp.glsl");
    warpShader.set("resolution", float(swidth), float(sheight));

    bloomShader = loadShader("basicBloom.glsl");
    bloomShader.set("iResolution", float(swidth), float(sheight));

    // setup warped stargate graphic
    warpedGraphic = createGraphics(swidth, sheight, P2D);
    warpedGraphic.textureWrap(REPEAT);
  }

  void update() {
    updateFlatView(flatView);
  }

  void updateFlatView(PGraphics g) {
    g.beginDraw();
    // setup
    g.colorMode(HSB, 1.0);
    // draw background
    g.fill(0.1);
    g.stroke(1, 0, 1);
    g.rect(0, 0, g.width, g.height);

    // determine zoom position
    int loopDuraiotn_ms_min = 100*1000;
    int loopDuraiotn_ms_max = 5*1000;
    int loopDuration_ms = 20*1000;
    float cx = g.width - (millis()%loopDuration_ms)*1f/loopDuration_ms * g.width;
    float cy = g.height/2;

    // draw scrolling background image
    g.image(flatViewBgTexture, cx, 0, swidth, sheight);
    g.image(flatViewBgTexture, cx-g.width, 0, swidth, sheight);

    // draw debug rectangle boundaries
    //  g.noFill(); g.rect(0, 0, g.width, g.height);

    // draw circle
    g.strokeWeight(5);
    g.noFill();
    float x, y;
    for (int i=0; i<10; i++) {
      x = i*80; //cx;
      y = cy;
      g.ellipse(x, y, 40, 40);
    }

    g.endDraw();
  }

  PGraphics getOutputGraphic() {

    // Warp Flat View
    warpedGraphic.beginDraw();
    warpShader.set("iMouse", float(mouseX), float(mouseY));
    //  warpShader.set("time", millis() / 1000.0); // update shader
    warpedGraphic.shader(warpShader);
    warpedGraphic.image(flatView.get(), 0, 0, swidth, sheight);
    warpedGraphic.endDraw();

    // Bloom
    if (!mousePressed) {
      warpedGraphic.beginDraw();
      warpedGraphic.resetShader();
      bloomShader.set("iMouse", float(mouseX), float(mouseY));
      warpedGraphic.shader(bloomShader);
      warpedGraphic.image(warpedGraphic, 0, 0);
      warpedGraphic.endDraw();
    }

    // Output
    return warpedGraphic;
  }
}

void draw() {
  background(122);

  // Update Stargate
  stargateDrawer.update();

  // Draw Stargate
  image(stargateDrawer.getOutputGraphic(), 0, 0);

  // draw framerate
  text(frameRate, 20, 20);
}

