/**
 * Deform. 
 * 
 * A GLSL version of the oldschool 2D deformation effect, by Inigo Quilez.
 * Ported from the webGL version available in ShaderToy:
 * http://www.iquilezles.org/apps/shadertoy/
 * (Look for Deform under the Plane Deformations Presets)
 * 
 */

// This is the same

PImage texture;
PShader myShader;

PGraphics wallGraphic;
int wallGraphicWidth = 512;
int wallGraphicHeight = 512;

int swidth = 1024; // make this flexible!
int sheight = 812;

void setup() {
  size(swidth, sheight, P2D);

  wallGraphic = createGraphics(wallGraphicWidth, wallGraphicHeight, P2D);

  //
  textureWrap(REPEAT);
  texture = loadImage("tex1.jpg");
  //
  myShader = loadShader("fly.glsl");
  println(width, height);
  myShader.set("resolution", float(swidth), float(sheight));
}

void draw() {
  background(122);

  myShader.set("time", millis() / 1000.0);

  // Note - Shaders do not seem to effect processing draw elements - perhaps would have to draw those to a 'texture'

  shader(myShader, LINES);
  //  image(texture, 0, 0, swidth, sheight);
  //  resetShader();



  drawWallGraphic(wallGraphic);
  //  image(wallGraphic,0,0);
  image(wallGraphic, 0, 0, swidth, sheight);
}

void drawWallGraphic(PGraphics g) {
  g.beginDraw();
  // setup
  g.colorMode(HSB, 1.0);
  //  g.background(0.0);
  
  // draw debug - boundaries
  g.rect(0, 0, g.width, g.height);
  
  // draw centre circle
  g.translate(g.width/2, g.height/2);
  g.ellipse(0, 0, 20, 20);
  
  g.endDraw();
}

