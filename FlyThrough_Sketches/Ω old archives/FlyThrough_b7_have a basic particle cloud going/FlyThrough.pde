
int swidth = 1280;
int sheight = 800;

StargateDrawer stargateDrawer;

void setup() {
  size(swidth, sheight, P2D);

  // Set the target framerate to ridiculously high to see how fast we can actually get this
  frameRate(1000);

  // setup Stargate
  stargateDrawer = new StargateDrawer(swidth, sheight);
}



class ParticleField {
  // Particle Field positions are normalised 0 to 1
  int numParticles = 100;
  ArrayList<Particle> particles = new ArrayList<Particle>();
  float xvelMin = -0.001;
  float xvelMax = -0.01;
  ParticleField() {
    for (int i=0; i<numParticles; i++) {
      Particle p = new Particle(random(1), random(1), xvelMin, xvelMax);
      particles.add(p);
    }
  }
  void update() {
    for (int i=0; i<numParticles; i++) {
      particles.get(i).update();
    }
  }
  ArrayList<Particle> getParticles() {
    return particles;
  }
}

class Particle {
  // Normalized particle positioning
  float x = 0;
  float y = 0;
  float xvel = 0;
  float xvelMin = 0;
  float xvelMax = 0;
  Particle(float _x, float _y, float _xvelMin, float _xvelMax) {
    x = _x;
    y = _y;
    xvelMin = _xvelMin;
    xvelMax = _xvelMax;
    xvel = random(Math.min(xvelMin, xvelMax), Math.max(xvelMin, xvelMax) );
  }
  void update() {
    x += xvel;
    // loop x around
    if (x < 0) {
      x = 1;
      xvel = random(Math.min(xvelMin, xvelMax), Math.max(xvelMin, xvelMax) );
    }
    //    if (x > 1) {
    //      x = 0;
    //      xvel = random(xvelMin, xvelMax);
    //    }
  }
}

class StargateDrawer {
  // NOTES
  // efficiency - framerate can be increased by changing scales, and drawing less pshapes (drawing textured images seems ok)
  // filter leaves a black circle near the horizon line, potentially because values are infinite near there
  // shaders and filters don't play nice with the pixels in PGraphic, they're fine with PImages though

  PGraphics flatView;
  PImage flatViewBgTexture; 
  PShader warpShader;
  PShader bloomShader;

  PGraphics warpedGraphic;

  ParticleField particleField;

  StargateDrawer(int swidth, int sheight) {

    // setup flatView
    int flatViewWidth = swidth; // these can be changed
    int flatViewHeight = sheight;
    flatView = createGraphics(flatViewWidth, flatViewHeight, P2D);
    flatView.textureWrap(REPEAT);
    flatView.smooth(8);

    flatViewBgTexture = loadImage("bannerTest2.jpg");

    // setup shaders
    warpShader = loadShader("stargateWarp.glsl");
    warpShader.set("resolution", float(swidth), float(sheight));

    bloomShader = loadShader("basicBloom.glsl");
    bloomShader.set("iResolution", float(swidth), float(sheight));

    // setup warped stargate graphic
    warpedGraphic = createGraphics(swidth, sheight, P2D);
    warpedGraphic.textureWrap(REPEAT);

    // setup particle field
    particleField = new ParticleField();
  }

  int loopDuration_ms = 20*1000;
  void update() {
    // Set Speed-Dependent Variables
    float mouseX_n = mouseX*1f/swidth;
    mouseX_n = Math.max(0, mouseX_n);
    mouseX_n = (float) (Math.pow(mouseX_n, 0.1)) ;
    // Update Textured Background's loop duration
    int[] loopDuration_ms_range = {
      100*1000, 5*1000
    };
    loopDuration_ms = (int)(map(mouseX_n, 0, 1, loopDuration_ms_range[0], loopDuration_ms_range[1]));

    // Update Warpe Scale
    // default is .001
    float[] warp_wrapScaleHeight_range = {
      0.08, 0.0005
    };
    float warp_wrapScaleHeight = map(mouseX_n, 0, 1, warp_wrapScaleHeight_range[0], warp_wrapScaleHeight_range[1]); 
    warpShader.set("wrapScaleHeight", warp_wrapScaleHeight);

    // Update Flat View
    updateFlatView(flatView);

    // Update Particles
    particleField.update();
  }

  void updateFlatView(PGraphics g) {
    g.beginDraw();
    // setup
    g.colorMode(HSB, 1.0);
    // draw background
    g.fill(0.1);
    //    g.stroke(1, 0, 1);
    g.noStroke();
    g.rect(0, 0, g.width, g.height);

    // determine zoom position

    float cx = g.width - (millis()%loopDuration_ms)*1f/loopDuration_ms * g.width;
    float cy = g.height/2;

    // draw scrolling background image
    //    g.image(flatViewBgTexture, cx, 0, swidth, sheight);
    //    g.image(flatViewBgTexture, cx-g.width, 0, swidth, sheight);

    // draw debug rectangle boundaries
    //  g.noFill(); g.rect(0, 0, g.width, g.height);

    // draw circle
    g.stroke(1, 0, 1);
    g.strokeWeight(1);
    g.noFill();
    float x, y;
    for (int i=0; i<10; i++) {
      x = i*80; //cx;
      y = cy;
      g.ellipse(x, y, 40, 40);
    }

    // draw particles onto flat view
    g.strokeWeight(2.0); // vary by speed
    for (Particle p : particleField.getParticles ()) {
      g.point(p.x * g.width, p.y * g.height);
    }

    g.endDraw();
  }

  PGraphics getOutputGraphic() {

    // Warp Flat View
    warpedGraphic.beginDraw();
    warpShader.set("iMouse", float(mouseX), float(mouseY));
    //  warpShader.set("time", millis() / 1000.0); // update shader
    if (mousePressed) {
      warpedGraphic.shader(warpShader);
    }
    warpedGraphic.image(flatView.get(), 0, 0, swidth, sheight);
    warpedGraphic.endDraw();

    // Bloom
    if (mousePressed) {
      warpedGraphic.beginDraw();
      warpedGraphic.resetShader();
      bloomShader.set("iMouse", float(mouseX), float(mouseY));
      warpedGraphic.shader(bloomShader);
      warpedGraphic.image(warpedGraphic, 0, 0);
      warpedGraphic.endDraw();
    }

    // Output
    return warpedGraphic;
  }
}

void draw() {
  background(122);

  // Update Stargate
  stargateDrawer.update();

  // Draw Stargate
  image(stargateDrawer.getOutputGraphic(), 0, 0);

  // draw framerate
  text(frameRate, 20, 20);
}

