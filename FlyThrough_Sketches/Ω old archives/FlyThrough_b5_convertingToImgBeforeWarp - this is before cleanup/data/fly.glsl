// IQ Reference: http://www.iquilezles.org/apps/shadertoy/index2.html - FLY
// Shader Toy References:
// https://www.shadertoy.com/view/4s2XWm#
// https://www.shadertoy.com/view/4sBSRy#

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

uniform sampler2D texture; //

uniform float time; // runtime in float seconds
uniform vec2 resolution; // (swidth, sheight) - pixels

void main(void) {
  float _speed_horizonRotation = 0.0; //0.25; // 0.025;
  float textureProgressionSpeed = 0.00; //.4; //.20;
  float wrapScaleWidth = .1; // determines the 'width' scale of the image layed down (smaller the value longer the img )
  float wrapScaleHeight = .001; // wrapScaleWidth; // determines the 'height' scale of the image layed down - for the repeating texture
  

  // gl_FragCoord.xy = pixel location in pixels

  // Get pixel location normalized as range [0,1]
  vec2 pixelPos_n = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;

  // Horizon Rotation
  float angle = time * _speed_horizonRotation;

  // ~? Texture Position Calculations 
  float x = pixelPos_n.x * cos(angle) - pixelPos_n.y * sin(angle);
  float y = pixelPos_n.x * sin(angle) + pixelPos_n.y * cos(angle);

  vec2 uv;
  float textureProgression = textureProgressionSpeed * time;
  // note: 1 / abs(y) - means its '1' at horizon edge, and infinite in between
  // note: swapped y and x to rotate image
  uv.y = 1.0 - (wrapScaleWidth * x / abs(y) + 0.5);  // offset by .5 so centred
  uv.x = textureProgression + wrapScaleHeight / abs(y); 

  // Determine Pixel Color
  vec3 pixelColor_col = texture2D(texture, uv).xyz;
  // pixelColor_col *= y*y; // this makes the texture visually fade into the z axis
  // pixelColor_col *= 0.09/abs(y); // this makes it get whiter towards the horizon
  // pixelColor_col *= max(0.1/abs(y), y*y); // combines the 2 above, making the horizon and the edges bright
  // pixelColor_col *= pow(abs(y), 0.6) * 10.0; // another way of making it darker near z axis
  float pixelColor_alpha = 1.0;
  
  // set the gl_FragColor - i.e. pixel color
  gl_FragColor = vec4(pixelColor_col, pixelColor_alpha);
}