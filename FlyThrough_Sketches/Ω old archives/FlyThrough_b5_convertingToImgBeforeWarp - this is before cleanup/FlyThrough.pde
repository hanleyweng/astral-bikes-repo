
int swidth = 1280;
int sheight = 720;

PImage texture;
PShader myShader;

PGraphics wallGraphic;
int wallGraphicWidth = swidth;
int wallGraphicHeight = sheight;

PGraphics stargateGraphic;

PImage bannerImage2;



boolean debugMode = false;

void setup() {
  size(swidth, sheight, P2D);

  //  frameRate(60);
  frameRate(1000);

  wallGraphic = createGraphics(wallGraphicWidth, wallGraphicHeight, P2D);
  
  wallGraphic.textureWrap(REPEAT);
  wallGraphic.smooth(8);
  //  wallGraphic.smooth(8);

  bannerImage2 = loadImage("bannerTest2.jpg");

  stargateGraphic = createGraphics(swidth, sheight, P2D);
  stargateGraphic.textureWrap(REPEAT);
  //  stargateGraphic.smooth(8);
  //
  //  textureWrap(REPEAT);
  //
  myShader = loadShader("fly.glsl");

  myShader.set("resolution", float(swidth), float(sheight));
}

int currentFrame = 0;

PImage tempImage;

void draw() {
  background(122);

  //  ellipse(swidth/2, sheight/2, 50, 50);

  ///////////////////////////////////////////////////////
  // Update Shader
  myShader.set("time", millis() / 1000.0);

  ///////////////////////////////////////////////////////
  // Draw
  //  if (!debugMode) shader(myShader);

  drawWallGraphic(wallGraphic);

  //  if (debugMode) {
  //    image(wallGraphic, 0, 0); // Note - Image must stretch canvas in order to be applied
  //  } else {
  //    image(wallGraphic, 0, 0, swidth, sheight);
  //  }
  //
  //  resetShader();

  stargateGraphic.beginDraw();
  stargateGraphic.shader(myShader);
  // note - in general - also much more efficient if scaled down
  tempImage = wallGraphic.get(); // drops from 50 to 36, drops from 200 to 84/45 -- Pgraphics drawing shapes uses up much more frames
  stargateGraphic.image(tempImage, 0, 0, swidth, sheight);
//  stargateGraphic.image(wallGraphic, 0, 0, swidth, sheight);
  // stargateGraphic.filter(myShader); // filter annoyingly leaves a little black circle near the horizon line
  stargateGraphic.endDraw();

  image(stargateGraphic, 0, 0);

  text(frameRate, 20, 20);

  ///////////////////////////////////////////////////////
  // Update
  currentFrame++;
}

void drawWallGraphic(PGraphics g) {
  g.beginDraw();
  g.colorMode(HSB, 1.0);
  // setup
  //  g.background(0.5); // background doesn't seem to work, drawing my own
  // draw debug - boundaries
  g.fill(0.1);
  g.stroke(1, 0, 1);
  g.rect(0, 0, g.width, g.height);

  // position
  int loopDuration_ms = 20*1000;
  float cx = g.width - (millis()%loopDuration_ms)*1f/loopDuration_ms * g.width;
  float cy = g.height/2;

  //  cx = 0;

  //  g.shader(myShader); // NOTE: In order for shader to nicely affect images ,those images can not be PGraphics!

  // draw image
  //  g.image(bannerImage2copycopy, 0, 0, swidth, sheight);
  g.image(bannerImage2, cx, 0, swidth, sheight);
  g.image(bannerImage2, cx-g.width, 0, swidth, sheight);

  //  g.resetShader();

  //  // draw rect
  //  g.noFill();
  //  g.rect(0, 0, g.width, g.height);

  // draw circle
  g.strokeWeight(5);
  g.noFill();
  //  g.translate(cx, cy);

  float x, y;
  for (int i=0; i<10; i++) {
    x = i*150; //cx;
    y = cy;
    g.ellipse(x, y, 100, 100);
  }

  g.endDraw();
}

void mouseClicked() {
  debugMode = !debugMode;
}

