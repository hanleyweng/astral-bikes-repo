int h = 256;
int w = 4096;

void setup () {
  size(w, h); 
  colorMode(HSB, 1.0);
  noLoop();
}

void gen1 () {
  background(0); 
  noStroke();
  
  float value = 1.0;
  fill(value);
  
  float thickness = int(random(1, 32)); 
  for (int r = 0; r < h; r+=thickness+1) {
    int weight = int(random(0, 25));
    for (int c = 0; c < w; ) {
      int n = int(random(-75, 25));
      int abs_n = abs(n);
      c += abs_n;
      
      if (weight + n < 0)
        value = 0.0;
      else
        value = 1.0;
      fill(value);
      rect(c, r, abs_n, thickness);
    }
    thickness = int(random(1, 32)); 
  } 
  
  save("image.png");
  exit();
}

void gen2 () {
  background(0); 
  noStroke();
  
  float value = 1.0;
  fill(value);
  
  float weight = int(random(0, 25)); 
  for (int c = 0; c < w;) {
    int n = int(random(-49, 50));
    weight += n;
    int abs_n = abs(n);
    c += abs_n;
    
    if (weight < 0)
      value = 0.0;
    else
      value = 1.0;
    fill(value);
    rect(c, 0, abs_n, h);
  }
  
  save("image.png");
  exit(); 
}

void gen3 () {
  background(0); 
  noStroke();
  
  float value = 1.0;
  fill(value);
  
  for (int r = 0; r < h; r+=int(random(-2, 4))) {
    for (int c = 0; c < w; c+=int(random(-4, 8))) {
      
      if ((
            ((r+1) * c/8 % 2)
//           * (r/2 % 2)
//           * (r/2 % 3)
//           * (r/2 % 5)
//           * (r/2 % 7)
           * (r/2 % 11)
//           * (c/8 % 3)
//           * (c/8 % 5)
//           * (c/8 % 7)
           * (c/8 % 11)
           * (c/8 % 13)
               == 0) || random(0, c) > w/8 )
        value = 0.0;
      else
        value = 1.0;
        
      fill(value);
      rect(c, r, c, r);
    }
  } 
  
  save("image.png");
  exit();
}

void draw () {
//  gen1();  
//  gen2();
  gen3();
}
