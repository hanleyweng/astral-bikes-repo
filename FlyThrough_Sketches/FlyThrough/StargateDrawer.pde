final int ixMode_MOUSE = 0;
final int ixMode_BIKE = 1;
int _SET_IXMODE = ixMode_BIKE; // set which input determines speed


class StargateDrawer {
  // NOTES
  // efficiency - framerate can be increased by changing scales, and drawing less pshapes (drawing textured images seems ok)
  // filter leaves a black circle near the horizon line, potentially because values are infinite near there
  // shaders and filters don't play nice with the pixels in PGraphic, they're fine with PImages though

  PGraphics flatViewGround; 
  PGraphics flatSky;

  PImage groundTextureA;
  PImage groundTextureB;
  PImage groundTextureC;

  PImage skyTextureA;
  PImage skyTextureB;
  PImage skyTextureC;

  PShader warpShader;
  PShader bloomShader;

  PGraphics warpedGroundGraphic;
  PGraphics warpedSkyGraphic;
  PGraphics bloomedGraphic;
  PGraphics outputGraphic;

  ParticleField groundParticleField;
  ParticleField skyParticleField;

  StargateDrawer(int swidth, int sheight) {

    // SETUP FLAT VIEWS
    flatViewGround = createGraphics(swidth, sheight, P2D); // these dimensions can be changed
    flatViewGround.textureWrap(REPEAT);
    flatViewGround.smooth(8);
    groundTextureA = loadImage("bannerA.jpg");
    groundTextureB = loadImage("bannerB.jpg");
    groundTextureC = loadImage("bannerC.jpg");

    flatSky = createGraphics(swidth, sheight, P2D); // these dimensions can be changed
    flatSky.textureWrap(REPEAT);
    flatSky.smooth(8);
    skyTextureA = loadImage("bannerA_sky.jpg");
    skyTextureB = loadImage("bannerB_sky.jpg");
    skyTextureC = loadImage("bannerC_sky.jpg");


    // setup shaders
    warpShader = loadShader("stargateWarp.glsl");
    warpShader.set("resolution", float(swidth), float(sheight));

    bloomShader = loadShader("basicBloom.glsl");
    bloomShader.set("iResolution", float(swidth), float(sheight));

    // Create Graphics
    warpedGroundGraphic = createGraphics(swidth, sheight, P2D);
    warpedGroundGraphic.textureWrap(REPEAT);

    warpedSkyGraphic = createGraphics(swidth, sheight, P2D);
    warpedSkyGraphic.textureWrap(REPEAT);

    bloomedGraphic = createGraphics(swidth, sheight, P2D);

    outputGraphic = createGraphics(swidth, sheight);

    // setup particle field
    groundParticleField = new ParticleField();
    skyParticleField = new ParticleField();
  }

  //////////////////////////////////////////////////////////////////
  // UPDATE

  void update() {

    // Set Speed-Dependent Variables

    switch(_SET_IXMODE) {

    case ixMode_MOUSE:
      float mouseX_n = mouseX*1f/swidth;
      mouseX_n = Math.max(0, mouseX_n);
      //    mouseX_n = (float) (Math.pow(mouseX_n, 0.1)) ;
      speedEffect_n = mouseX_n;
      break;
    case ixMode_BIKE:
      speedEffect_n = ix_speedEffect_n;
      // speedEffect_n = (float) (Math.pow(ix_speedEffect_n, 0.5));
      break;
    }

    /////////////////////////////////////////////////////////////////////////////////
    // Update Warp Scale
    // default is .001
    float warp_wrapScaleHeight_min = 0.0150; // 0.0800
    float warp_wrapScaleHeight_max = 0.0030; // 0.0005
    float warp_wrapScaleHeight = map(speedEffect_n, 0, 1, warp_wrapScaleHeight_min, warp_wrapScaleHeight_max);
    //    warp_wrapScaleHeight = 0.0150; //0.0300; //0.005;
    warpShader.set("textureScaleWidth", warp_wrapScaleHeight);

    /////////////////////////////////////////////////////////////////////////////////
    // Update Flat Views
    updateFlatView(flatSky, 0);
    updateFlatView(flatViewGround, 1);

    /////////////////////////////////////////////////////////////////////////////////
    // UPDATE CANVAS X POSITION

    int canvasWidthBound = groundTextureA.width+groundTextureB.width+groundTextureC.width;  // 48960

    // canvas velocity
    // awesome - we can choose our canvasXscale independently of the canvasXvelocity - therefore loop duration is determined by canvasXvelocity, not by canvasXscale
    // at vel = 1 pixel/frame - this would take 40.8 minutes to view entire animation - (48960f/20fps)
    // at vel = 20.4 - the entire animation will be viewed in 2 minutes (meaning average riders - half that - will see it in ~4mins)
    float canvasXvel_min = 1f/canvasXscale;
    float canvasXvel_max = 21;  
    float canvasXvelocity = map(speedEffect_n, 0, 1, canvasXvel_min, canvasXvel_max);
    canvasXvelocity = Math.max(canvasXvelocity, 1f/canvasXscale); // for smooth texture movements, the minimum canvasXvelocity must be the inverse of the canvasXscale.
    //    canvasXvelocity = 20.4; //
    //    canvasXvelocity = 10.2; //

    // canvas position
    canvasX -= canvasXvelocity;
    while (canvasX < -canvasWidthBound) {
      canvasX += canvasWidthBound;
    }
  }

  //////////////////////////////////////////////////////////////////
  // UPDATE FLAT VIEW

  float canvasX = 0;
  float canvasXscale = 0.5; //0.5; //3.0; 

  void updateFlatView(PGraphics g, int planeType) {
    //
    ParticleField particleField = (planeType==0) ? skyParticleField : groundParticleField;
    particleField.update();

    // planeType = 0 for sky, = 0 for ground
    g.beginDraw();
    g.colorMode(HSB, 1.0);
    g.fill(0.1);
    g.noStroke();
    g.rect(0, 0, g.width, g.height);

    // scale
    g.scale(canvasXscale, 1.0);
    // translate
    g.pushMatrix();
    g.translate(canvasX, 0);
    // draw entire mural
    if (planeType == 0) {
      // sky
      g.image(skyTextureA, 0, 0, skyTextureA.width, g.height);
      g.image(skyTextureB, skyTextureA.width, 0, skyTextureB.width, g.height);
      g.image(skyTextureC, skyTextureA.width + skyTextureB.width, 0, skyTextureC.width, g.height);
      g.image(skyTextureA, skyTextureA.width + skyTextureB.width + skyTextureC.width, 0, skyTextureA.width, g.height); // wrap around image
    } else {
      // ground
      g.image(groundTextureA, 0, 0, groundTextureA.width, g.height);
      g.image(groundTextureB, groundTextureA.width, 0, groundTextureB.width, g.height);
      g.image(groundTextureC, groundTextureA.width + groundTextureB.width, 0, groundTextureC.width, g.height);
      g.image(groundTextureA, groundTextureA.width + groundTextureB.width + groundTextureC.width, 0, groundTextureA.width, g.height); // wrap around image
    }
    g.popMatrix();

    ///////////////////////////////////////////////////////////////////////////
    // draw particles onto flat view
    g.stroke(1, 0, 1);
    g.strokeWeight(2.0); // vary by speed
    ArrayList<Particle> particles = particleField.getParticles();
    for (Particle p : particles) {
      g.point(p.x * g.width, p.y * g.height);
    }

    g.endDraw();
  }

  //////////////////////////////////////////////////////////////////
  // DEBUG MODE
  String[] debugModes = {
    "normal", "flatviewGround"
  };
  int debugMode = 0;
  //////////////////////////////////////////////////////////////////
  void mouseReleased() {
    debugMode += 1;
    debugMode %= debugModes.length;
  }

  //////////////////////////////////////////////////////////////////
  // GET OUTPUT GRAPHIC
  PGraphics getOutputGraphic() {

    String curDebugMode = debugModes[debugMode];

    if (curDebugMode.equals("flatviewGround")) {
      outputGraphic.beginDraw();
      outputGraphic.image(flatViewGround.get(), 0, 0, swidth, sheight/2);
      outputGraphic.image(flatSky.get(), 0, sheight/2, swidth, sheight/2);
      outputGraphic.endDraw();
      return outputGraphic;
    }

    // Warp Flat View
    // update warp shader
    warpShader.set("iMouse", float(mouseX), float(mouseY));
    warpShader.set("time", millis() / 1000.0); // update shader

    warpedGroundGraphic.beginDraw();
    warpShader.set("mode", 1);
    warpedGroundGraphic.shader(warpShader);
    warpedGroundGraphic.image(flatViewGround.get(), 0, 0, swidth, sheight);
    warpedGroundGraphic.endDraw();

    warpedSkyGraphic.beginDraw();
    warpShader.set("mode", 0);
    warpedSkyGraphic.shader(warpShader);
    warpedSkyGraphic.image(flatSky.get(), 0, 0, swidth, sheight);
    warpedSkyGraphic.endDraw();

    // Bloom
    bloomedGraphic.beginDraw();
    bloomShader.set("iMouse", float(mouseX), float(mouseY));
    bloomedGraphic.shader(bloomShader);
    bloomedGraphic.image(warpedGroundGraphic, 0, 0);
    bloomedGraphic.image(warpedSkyGraphic, 0, 0);
    bloomedGraphic.endDraw();

    // Output
    return bloomedGraphic;
  }
}

