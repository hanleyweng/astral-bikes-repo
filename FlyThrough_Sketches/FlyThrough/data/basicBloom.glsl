// IQ Reference: http://www.iquilezles.org/apps/shadertoy/index2.html - FLY
// Shader Toy References:
// https://www.shadertoy.com/view/Ms2Xz3

#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

#define PROCESSING_TEXTURE_SHADER

uniform sampler2D texture; //

uniform float time; // runtime in float seconds
uniform vec2 iResolution; // (swidth, sheight) - pixels
uniform vec2 iMouse; // mouse position - pixels

vec4 BlurColor (in vec2 Coord, in sampler2D Tex, in float MipBias)
{
  // MipBias is Blur Size

  // vec2 TexelSize = MipBias/iChannelResolution[0].xy;
  vec2 TexelSize = MipBias/iResolution.xy; // for here, texture and canvas resolutions are the same

  vec4  Color = texture2D(Tex, Coord, MipBias);
  Color += texture2D(Tex, Coord + vec2(TexelSize.x,0.0), MipBias);      
  Color += texture2D(Tex, Coord + vec2(-TexelSize.x,0.0), MipBias);     
  Color += texture2D(Tex, Coord + vec2(0.0,TexelSize.y), MipBias);      
  Color += texture2D(Tex, Coord + vec2(0.0,-TexelSize.y), MipBias);     
  Color += texture2D(Tex, Coord + vec2(TexelSize.x,TexelSize.y), MipBias);      
  Color += texture2D(Tex, Coord + vec2(-TexelSize.x,TexelSize.y), MipBias);     
  Color += texture2D(Tex, Coord + vec2(TexelSize.x,-TexelSize.y), MipBias);     
  Color += texture2D(Tex, Coord + vec2(-TexelSize.x,-TexelSize.y), MipBias);    

  return Color/9.0;
}

void main(void) {

  // set uv to texture exactly
  vec2 uv = (gl_FragCoord.xy/iResolution.xy); 
  uv.y = 1.0-uv.y; // flip y axis

  // Set Bloom Settings
  float Threshold = 0.0+iMouse.y/iResolution.y*1.0;
  float Intensity = 1.0-iMouse.x/iResolution.x*1.0;
  Intensity *= 2.0;
  float BlurSize = 1.0-iMouse.x/iResolution.x*1.0;
  BlurSize *= 6.0;

  Threshold = -0.0;
  Intensity = 0.9;
  BlurSize = 4.0;

  // Set Bloom
  vec4 Highlight = clamp(BlurColor(uv, texture, BlurSize)-Threshold,0.0,1.0)*1.0/(1.0-Threshold);

  // Set Pixel Color
  vec4 Color = texture2D(texture, uv); // pixel color

  Color *= 2.0; //5.0; //2.0; // multiply colors in brightness

  // Set Pixel Color
  gl_FragColor = 1.0-(1.0-Color)*(1.0-Highlight*Intensity); //Screen Blend Mode

}