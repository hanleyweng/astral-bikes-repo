final boolean _DEBUG_DRAWTEXT = false;

// SKETCH
int swidth = 1280;
int sheight = 800;

StargateDrawer stargateDrawer;

float speedEffect_n = 0;

void setup() {
  size(swidth, sheight, P2D);

  // Set the target framerate to ridiculously high to see how fast we can actually get this
  frameRate(1000);

  // setup Stargate
  stargateDrawer = new StargateDrawer(swidth, sheight);

  // setup arduino
  setupArduino();

  //
  textSize(64);
  fill(255);
}

void draw() {
  //  background(122);
  //  background(0, 180, 180);
  background(0);

  // Update Speed IX values from Arduino
  updateFromArduino();

  // Update Stargate
  stargateDrawer.update();

  // Draw Stargate
  image(stargateDrawer.getOutputGraphic(), 0, 0);


  if (_DEBUG_DRAWTEXT) {
    // draw framerate
    textAlign(LEFT, TOP);
    textSize(16);
    text(frameRate, 20, 20);

    // draw speed effect
    textAlign(RIGHT, TOP);
    textSize(32);
    text(speedEffect_n, swidth-20, 20);
  }
}

void mouseReleased() {
  stargateDrawer.mouseReleased();
}

