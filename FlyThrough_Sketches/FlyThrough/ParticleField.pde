class ParticleField {
  // Particle Field positions are normalised 0 to 1
  int numParticles = 5; //100;
  ArrayList<Particle> particles = new ArrayList<Particle>();
  float xvelMin = -0.001;
  float xvelMax = -0.01;
  ParticleField() {
    for (int i=0; i<numParticles; i++) {
      Particle p = new Particle(random(1), random(1), xvelMin, xvelMax);
      particles.add(p);
    }
  }
  void update() {
    for (int i=0; i<numParticles; i++) {
      particles.get(i).update();
    }
  }
  ArrayList<Particle> getParticles() {
    return particles;
  }
}

class Particle {
  // Normalized particle positioning
  float x = 0;
  float y = 0;
  float xvel = 0;
  float xvelMin = 0;
  float xvelMax = 0;
  Particle(float _x, float _y, float _xvelMin, float _xvelMax) {
    x = _x;
    y = _y;
    xvelMin = _xvelMin;
    xvelMax = _xvelMax;
    xvel = random(Math.min(xvelMin, xvelMax), Math.max(xvelMin, xvelMax) );
  }
  void update() {
    x += xvel;
    // loop x around
    if (x < 0) {
      x = 1;
      xvel = random(Math.min(xvelMin, xvelMax), Math.max(xvelMin, xvelMax) );
    }
    if (x > 1) {
      x = 0;
      xvel = random(xvelMin, xvelMax);
    }
  }
}
